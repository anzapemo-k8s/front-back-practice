from flask import Flask, render_template
import requests

app = Flask(__name__);

@app.route("/")
def front():
	return render_template("main.html")

@app.route("/back")
def back():
	r = requests.get("http://backend-service/test")
	return r.content

if __name__ == '__main__':
	app.run(port=80, debug=True)