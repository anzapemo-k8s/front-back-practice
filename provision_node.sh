#!/bin/bash

yum install -y epel-release
yum install -y docker
yum install -y htop
yum install -y tree
systemctl enable docker.service && systemctl start docker.service

#Minikube
#curl -Lo minikube https://storage.googleapis.com/minikube/releases/v0.28.2/minikube-linux-amd64 && chmod +x minikube && sudo mv minikube /usr/local/bin/

#Kubectl
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kube*
EOF
setenforce 0
yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes
systemctl enable kubelet && systemctl start kubelet
yum update -y

#!!!!!!!!!!!!!!!!!!!!!!!!!!!#
# REBOOT HERE WITH          #
# reboot                    #
#!!!!!!!!!!!!!!!!!!!!!!!!!!!#

# Turn off swap
swapoff -a
echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables

# echo "KUBELET_EXTRA_ARGS=--node-ip=192.103.1.4" > /etc/sysconfig/kubelet
# systemctl restart kubelet
# Execute the kubeadm join command