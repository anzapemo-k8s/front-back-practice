#!/bin/bash

hostname kmaster
yum install -y epel-release
yum install -y docker
yum install -y htop
yum install -y tree
systemctl enable docker.service && systemctl start docker.service

#Minikube
#curl -Lo minikube https://storage.googleapis.com/minikube/releases/v0.28.2/minikube-linux-amd64 && chmod +x minikube && sudo mv minikube /usr/local/bin/

#Kubectl
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kube*
EOF
setenforce 0
yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes
systemctl enable kubelet && systemctl start kubelet
yum update -y

#!!!!!!!!!!!!!!!!!!!!!!!!!!!#
# REBOOT HERE WITH          #
# reboot                    #
#!!!!!!!!!!!!!!!!!!!!!!!!!!!#

# Turn off swap
swapoff -a
echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables
# For Flannel
kubeadm init --pod-network-cidr=10.244.0.0/16 --apiserver-advertise-address=192.103.1.3

# Save the token to join

# kubeadm join 192.103.1.3:6443 --token c1v8gd.laj2o30y57aaiuvn --discovery-token-ca-cert-hash sha256:0b92be949e10e4d51e861ef54ca9aafc87b9d9d6ec3515c4958f29a55b2d6f9c

export KUBECONFIG=/etc/kubernetes/admin.conf
# Apply the flannel config
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/v0.10.0/Documentation/kube-flannel.yml
